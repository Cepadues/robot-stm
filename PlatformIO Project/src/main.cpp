#include <Arduino.h>
#include <Wire.h>

// Function declaration
void PID_Control();
void readValues();
double readLine();
void setSpeed(double);

// Main parameters
const uint8_t NUM_SENSORS = 4;
const uint16_t THRESHOLD = 400;
const uint16_t STOP_DURATION = 3000;
const uint16_t maxPos = (NUM_SENSORS - 1) * 1000;

// Motors parameters
const uint8_t baseSpeedA = 128;
const uint8_t baseSpeedB = 138;
const uint8_t maxSpeed = 160; // maxSpeed = (6V / VBAT) * 255

// I2C address of the PCF8574 in control of the motors
#define I2C_MOTORS 0x20

// If true, the robot will not move
bool DEBUG = false;

int pwmA = 5; // Pin for speed control
int pwmB = 6;
int STBY = 12; // Pin for stby control

int dir = B1010; // Control which way the wheels turn

// PID parameters
double KP = 5;
double KI = 0;
double KD = 0;

// Error and last error
double error = 0;
double lastError = 0;

// PID variables
double P = 0;
double I = 0;
double D = 0;

// Other variables
unsigned int sensorValues[NUM_SENSORS];
int onLine = 0;
int detectStop = 0;
double lastLine = maxPos / 2;

void setup()
{
  if (DEBUG)
  {
    Serial.begin(9600);
  }

  pinMode(pwmA, OUTPUT);
  pinMode(pwmB, OUTPUT);
  pinMode(STBY, OUTPUT);

  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);

  analogWrite(pwmA, baseSpeedA);
  analogWrite(pwmB, baseSpeedB);

  Wire.begin();
  Wire.beginTransmission(I2C_MOTORS);
  Wire.write(dir);
  Wire.endTransmission();
}

void loop()
{
  if (DEBUG)
  {
    Serial.write(27);
    Serial.print("[2J");
    Serial.write(27);
    Serial.print("[H");
  }
  
  PID_Control(); 

  if (detectStop == NUM_SENSORS || DEBUG)
  {
    digitalWrite(STBY, LOW);
  }
  else
  {
    digitalWrite(STBY, HIGH);
  }

  if (DEBUG)
  {
    Serial.flush();
  }
}

void readValues()
{
  detectStop = 0;
  onLine = 0;

  if (DEBUG)
  {
    Serial.println("Sensors value :");
  }
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    sensorValues[i] = analogRead(i);
    if (DEBUG)
    {
      Serial.print(sensorValues[i]);
      Serial.print('\t');
    }

    if (sensorValues[i] > THRESHOLD)
    {
      sensorValues[i] = 0; // 0 if on white
    }
    else
    {
      sensorValues[i] = 1; // 1 if on black
      onLine = 1;
    }
    detectStop += sensorValues[i]; // 0 if on a black horizontal line
  }
  if (DEBUG)
  {
    Serial.println("");
    for (int i = 0; i < NUM_SENSORS; i++)
    {
      Serial.print(sensorValues[i]);
      Serial.print('\t');
    }
    Serial.println("");
  }
}

double readLine()
{
  // From QTRSensors library
  double avg = 0;
  double sum = 0;
  readValues();

  for (int i = 0; i < NUM_SENSORS; i++)
  {
    avg += (double)sensorValues[i] * (i * 1000);
    sum += sensorValues[i];
  }
  avg = avg / sum;
  if (DEBUG) {
    Serial.println("Position data : ");
    Serial.print("On Line ");
    Serial.print(onLine);
    Serial.print('\t');
    Serial.print("AVG ");
    Serial.print(avg);
    Serial.print('\t');
    Serial.print("LAVG ");
    Serial.print(lastLine);
    Serial.print('\t');
    Serial.print("Sum ");
    Serial.print(sum);
    Serial.print('\t');
    Serial.print("MaxPos ");
    Serial.print(maxPos);
    Serial.println("");
  }

  // If line has been lost
  if (!onLine)
  {
    // If the line has been seen on the left
    if (lastLine < maxPos / 2)
    {
      return 0; // Then go all left
    }
    else
    {
      return maxPos; // Else go all right
    }
  }

  lastLine  = avg;
  return lastLine;
}

void setSpeed(double Correction)
{
  // Apply correction to the motor correction speed
  int motorSpeedA = baseSpeedA - (int)Correction;
  int motorSpeedB = baseSpeedB + (int)Correction;

  if (DEBUG) {
    Serial.println("Motors : ");
    Serial.print("MSA ");
    Serial.print(motorSpeedA);
    Serial.print('\t');
    Serial.print("MSB ");
    Serial.print(motorSpeedB);
    Serial.print('\t');
    Serial.print("Stop ");
    Serial.print(detectStop);
    Serial.println("");
  }

  // Checks so the motor doesn't get damaged
  if (motorSpeedA > maxSpeed)
  {
    motorSpeedA = maxSpeed;
  }
  if (motorSpeedB > maxSpeed)
  {
    motorSpeedB = maxSpeed;
  }
  if (motorSpeedA < 0)
  {
    motorSpeedA = 0;
  }
  if (motorSpeedB < 0)
  {
    motorSpeedB = 0;
  }

  // Write the new speed
  analogWrite(pwmA, motorSpeedA);
  analogWrite(pwmB, motorSpeedB);
}

void PID_Control() {
  double position = readLine();
  double error = (((double)maxPos / 2.0) - position)/100.0;

  P = error;
  I = I + error;
  D = error - lastError;

  lastError = error;

  double motorCorrection = (P * KP + I * KI + D*KD);

  if (DEBUG) {
    Serial.println("PID : ");
    Serial.print("Position ");
    Serial.print(position);
    Serial.print('\t');
    Serial.print("Error ");
    Serial.print(error);
    Serial.print('\t');
    Serial.print("P ");
    Serial.print(P);
    Serial.print('\t');
    Serial.print("I ");
    Serial.print(I);
    Serial.print('\t');
    Serial.print("D ");
    Serial.print(D);
    Serial.print('\t');
    Serial.print("LError ");
    Serial.print(lastError);
    Serial.print('\t');
    Serial.print("Corr ");
    Serial.print(motorCorrection);
    Serial.println("");
  }

  setSpeed(motorCorrection);
}