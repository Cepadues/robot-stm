## Line following robot with STM32 Nucleo Board

## Description
The goal of the project was to transform a line follower robot based on Arduino and made by a group of 1st year students into a STM32 Nucleo based robot. But we went beyond this goal, we wanted to totally rebuild the robot ourselves in order to consolidate our mechanical and electronic skills using mainly recycled material. This repository contains for the moment only the computer code, it is a Platform IO project based on the Arduino platform and implementing a PID algorithm (Proportional, Integral, Derivative).

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
To install this project, you should already have a fully working Platform IO on VSCode installation for STM32 boards using the Arduino platform. 

```bash
git clone https://gitlab.com/Cepadues/robot-stm.git
cd "robot-stm/PlatformIO Project"
code .
```
